import random
import requests
from helpers import db_helper
from helpers import util


COMPLEXITY_ROUTINES = [util.get_random_lower_char, util.get_random_digit,
                       util.get_random_upper_char, util.get_random_punctuation]


def generate_password(length: int, complexity: int) -> str:
    """Generate a random password with given length and complexity

        Complexity levels:
            Complexity == 1: return a password with only lowercase chars
            Complexity ==  2: Previous level plus at least 1 digit
            Complexity ==  3: Previous levels plus at least 1 uppercase char
            Complexity ==  4: Previous levels plus at least 1 punctuation char

        :param length: number of characters
        :param complexity: complexity level
        :returns: generated password
    """

    if complexity > 4 or complexity < 1:
        raise Exception("Complexity must be between 1 and 4")

    if length < complexity - 1:
        raise Exception("Complexity %d must have a length of at least %d" % (complexity, complexity - 1))

    required_chars = ""
    pwd = ""

    # get at least one required char from each complexity level, 1 is not necessary because it is not required
    for i in range(1, complexity):
        required_chars += COMPLEXITY_ROUTINES[i]()

    # join required chars with any others that satisfy complexity level
    for i in range(length - len(required_chars)):
        pwd += COMPLEXITY_ROUTINES[random.randint(0, complexity - 1)]()

    pwd += required_chars
    pwd = util.shuffle_str(pwd)

    return pwd


def check_password_level(password: str) -> int:
    """Return the password complexity level for a given password

        Complexity levels:
            Return complexity 1: If password has only lowercase chars
            Return complexity 2: Previous level condition and at least 1 digit
            Return complexity 3: Previous levels condition and at least 1 uppercase char
            Return complexity 4: Previous levels condition and at least 1 punctuation

        Complexity level exceptions (override previous results):
            Return complexity 2: password has length >= 8 chars and only lowercase chars
            Return complexity 3: password has length >= 8 chars and only lowercase and digits

        :param password: password
        :returns: complexity level
    """

    comp = 1

    for i in range(len(password)):
        char = password[i]
        if util.is_punctuation(char):
            return 4
        elif util.is_letter(char) and char.isupper():
            comp = 3
        elif util.is_digit(char) and comp == 1:
            comp = 2

    return comp


def create_user(db_path: str) -> None:  # you may want to use: http://docs.python-requests.org/en/master/
    """Retrieve a random user from https://randomuser.me/api/
    and persist the user (full name and email) into the given SQLite db

    :param db_path: path of the SQLite db file (to do: sqlite3.connect(db_path))
    :return: None
    """

    response = requests.get("https://randomuser.me/api/").json()

    user = response["results"][0]

    name = ("%s %s" % (user["name"]["first"], user["name"]["last"])).title()
    email = user["email"]

    db_helper.connect_db(db_path)

    db_helper.create_users_table()
    db_helper.insert_user(name, email)

    db_helper.close_connection()
