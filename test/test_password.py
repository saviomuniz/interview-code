import solution
import random

MAX_LENGTH = 500
NUMBER_OF_GENERATED_PASSWORDS = 10000


def test_password_complexity():
    success = True

    for i in range(0, NUMBER_OF_GENERATED_PASSWORDS):
        complexity = random.randint(1, 4)

        testing_pwd = solution.generate_password(random.randint(3, MAX_LENGTH), complexity)
        success = solution.check_password_level(testing_pwd) == complexity

        if not success:
            break

    print("SUCCESS" if success else "FAIL")


if __name__ == "__main__":
    test_password_complexity()
