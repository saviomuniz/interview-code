import solution
from helpers import db_helper
import random

DB_PATH = "sample.db"
ROW_ID_INDEX = 0


def test_user_password_generate():
    db_helper.connect_db(DB_PATH)
    users = db_helper.get_last_n_users(10)

    for user in users:
        password = solution.generate_password(random.randint(6, 12), random.randint(1, 4))
        db_helper.update_user_password(password, user[ROW_ID_INDEX])

    db_helper.close_connection()


if __name__ == "__main__":
    for i in range(10):
        solution.create_user(DB_PATH)

    test_user_password_generate()

    db_helper.connect_db(DB_PATH)

    print(db_helper.get_last_n_users(10))

    db_helper.close_connection()
