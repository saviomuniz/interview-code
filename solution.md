
# Nexus Edge Interview Solution

## 1. Password generation

The first problem to be solved in the purposed activity is to generate passwords with different levels of complexity. For this purpose, I started the solution instantiating two constant (for letter and punctuation indication) and a few helper functions:


```python
import random

CHARS = "abcdefghijklmnopqrstuvwxyz"
PUNCTUATION_CHARS = "!#$%&()*+,-./:;<=>?[\]^_`{¦}~"

def is_letter(char: str) -> bool:
    return char.lower() in CHARS


def is_punctuation(char: str) -> bool:
    return char in PUNCTUATION_CHARS


def is_digit(char: str) -> bool:
    return char in [str(i) for i in range(10)]

def shuffle_str(string: str) -> str:
    return "".join(random.sample(string, len(string)))
```

After that, I created functions with routines for each password complexity level, each one of them retrieves one char corresponding to the complexity level:

For complexity 1:


```python
def get_random_lower_char() -> str:
    return CHARS[random.randint(0, len(CHARS) - 1)]
```

Complexity 2:


```python
def get_random_digit() -> str:
    return str(random.randint(0, 9))
```

Complexity 3:


```python
def get_random_upper_char() -> str:
    return get_random_lower_char().upper()
```

And complexity 4:


```python
def get_random_punctuation() -> str:
    return PUNCTUATION_CHARS[random.randint(0, len(PUNCTUATION_CHARS) - 1)]
```

Then I create one array mapping each complexity function to an index. I did that for two reasons: 1 - to enhance code neatness; 2 - to take advantage of the list order, which helped me to respect hierarchy as complexity level grows


```python
COMPLEXITY_ROUTINES = [get_random_lower_char, get_random_digit, get_random_upper_char, get_random_punctuation]
```

With all that defined, I could then implement the generate_password function:


```python
def generate_password(length: int, complexity: int) -> str:
    """Generate a random password with given length and complexity

        Complexity levels:
            Complexity == 1: return a password with only lowercase chars
            Complexity ==  2: Previous level plus at least 1 digit
            Complexity ==  3: Previous levels plus at least 1 uppercase char
            Complexity ==  4: Previous levels plus at least 1 punctuation char

        :param length: number of characters
        :param complexity: complexity level
        :returns: generated password
    """

    required_chars = ""
    pwd = ""

    # get at least one required char from each complexity level, 1 is not necessary because it is not required
    for i in range(1, complexity):
        required_chars += COMPLEXITY_ROUTINES[i]()

    # join required chars with any others that satisfy complexity level
    for i in range(length - len(required_chars)):
        pwd += COMPLEXITY_ROUTINES[random.randint(0, complexity - 1)]()

    pwd += required_chars
    pwd = shuffle_str(pwd)

    return pwd
```

Notice that to ensure that every complexity level has its required chars, I first create a string with only required chars, this required_chars string will have *complexity - 1* length, because it'll iterate over each complexity required char, except for Complexity 1, because it doesn't have any.

After that I create another string to complete the remaining length and I can then use any complexity routine below the current complexity (for instance, if I'm using complexity 3, I can use complexity 1 and 2 routines (get_random_digit, get_random_lower_char) as well as complexity 3. 

I then join the required_chars and the remaining string password and create the password.

At last I shuffle the password so that I required_chars get mixed with others

You can find below some passwords generated:


```python
generate_password(20, 4)
```




    '%h850f4W;Rn$9]7,o2Rr'




```python
generate_password(15, 3)
```




    '040QbV48lxRlu64'




```python
generate_password(10, 2)
```




    'zt8w8ilg24'




```python
generate_password(5, 1)
```




    'svhtx'



### Password complexity assertion

The second part of the password problem involved detecting a password complexity. This was easily done using the helper functions previously defined:


```python
def check_password_level(password: str) -> int:
    """Return the password complexity level for a given password

        Complexity levels:
            Return complexity 1: If password has only lowercase chars
            Return complexity 2: Previous level condition and at least 1 digit
            Return complexity 3: Previous levels condition and at least 1 uppercase char
            Return complexity 4: Previous levels condition and at least 1 punctuation

        Complexity level exceptions (override previous results):
            Return complexity 2: password has length >= 8 chars and only lowercase chars
            Return complexity 3: password has length >= 8 chars and only lowercase and digits

        :param password: password
        :returns: complexity level
    """

    comp = 1

    for i in range(len(password)):
        char = password[i]
        if is_punctuation(char):
            return 4
        elif is_letter(char) and char.isupper():
            comp = 3
        elif is_digit(char) and comp == 1:
            comp = 2

    return comp
```

I started assuming that the complexity of the password will be 1. I then iterate over every char of the password, and only change the complexity variable if I reach a char that belongs to a higher complexity category. Notice that complexity 4 is max, therefore if we find a punctuation I immediatly return 4.

Some execution samples are listed below:


```python
check_password_level('%h850f4W;Rn$9]7,o2Rr')
```




    4




```python
check_password_level('040QbV48lxRlu64')
```




    3




```python
check_password_level('zt8w8ilg24')
```




    2




```python
check_password_level('svhtx')
```




    1



At last, we can test if solution is work using one function to validate the other:


```python
complexity = random.randint(1,4)
assert(check_password_level(generate_password(50, complexity)) == complexity)
```

## 2. Random users creation

The second part of the activity involved creating random users and setting random passwords for those retrived users. I started this part of the problem defining some database helper functions that would help managing the users, I defined the database connection globally since it won't change and there won't be concurrent access in this example:


```python
import requests
import sqlite3

CONN = None


def create_users_table():
    if CONN is None:
        return

    cursor = CONN.cursor()
    cursor.execute("CREATE TABLE IF NOT EXISTS users (name TEXT, email TEXT, password TEXT)")
    CONN.commit()


def insert_user(name, email):
    if CONN is None:
        return

    cursor = CONN.cursor()
    cursor.execute("INSERT INTO users (name, email) VALUES ('%s', '%s')" % (name, email))
    CONN.commit()


def get_last_n_users(limit):
    if CONN is None:
        return

    return [user for user in CONN.cursor().execute('SELECT rowid, * FROM users ORDER BY rowid DESC LIMIT %d' % limit)]


def update_user_password(password, user_id):
    if CONN is None:
        return

    CONN.cursor().execute("UPDATE users SET password='%s' WHERE rowid=%d" % (password, user_id))
    CONN.commit()


def connect_db(db_path):
    global CONN

    if CONN is not None:
        return

    CONN = sqlite3.connect(db_path)


def close_connection():
    global CONN

    CONN.close()

    CONN = None
```

The create_users_table function will create an users' table if it has not already been created. The insert user will persist an user but with no password yet defined, which is update_user_password's responsibility. The connect_db and close_connection functions are used to open and close connection with the database.

With those helper functions in hand, writing the create_user function was a matter of retrieving and formatting information fromt the **randomuser.me** API and then writing it to the db.


```python
def create_user(db_path: str) -> None:  # you may want to use: http://docs.python-requests.org/en/master/
    """Retrieve a random user from https://randomuser.me/api/
    and persist the user (full name and email) into the given SQLite db

    :param db_path: path of the SQLite db file (to do: sqlite3.connect(db_path))
    :return: None
    """

    response = requests.get("https://randomuser.me/api/").json()

    user = response["results"][0]

    name = ("%s %s" % (user["name"]["first"], user["name"]["last"])).title()
    email = user["email"]

    connect_db(db_path)

    create_users_table()
    insert_user(name, email)

    close_connection()
```

We may now check its correctness:


```python
create_user("sample.db")
```


```python
connect_db("sample.db")
print(get_last_n_users(1))
close_connection()
```

    [(7, 'Gezina Plantinga', 'gezina.plantinga@example.com', None)]


### Generate a random password for a created user

At last, we may now update an user setting its password to one generated with a random length between 6 and 12 and random complexity:


```python
ROW_ID_INDEX = 0

connect_db("sample.db")
user = get_last_n_users(1)[0]
 
password = generate_password(random.randint(6, 12), random.randint(1, 4))
update_user_password(password, user[ROW_ID_INDEX])

print(get_last_n_users(1))
close_connection()
```

    [(7, 'Gezina Plantinga', 'gezina.plantinga@example.com', 'jfgtogwub')]

