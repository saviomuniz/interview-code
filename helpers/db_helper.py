import sqlite3

CONN = None


def create_users_table():
    if CONN is None:
        return

    cursor = CONN.cursor()
    cursor.execute("CREATE TABLE IF NOT EXISTS users (name TEXT, email TEXT, password TEXT)")
    CONN.commit()


def insert_user(name, email):
    if CONN is None:
        return

    cursor = CONN.cursor()
    cursor.execute("INSERT INTO users (name, email) VALUES ('%s', '%s')" % (name, email))
    CONN.commit()


def get_last_n_users(limit):
    if CONN is None:
        return

    return [user for user in CONN.cursor().execute('SELECT rowid, * FROM users ORDER BY rowid DESC LIMIT %d' % limit)]


def update_user_password(password, user_id):
    if CONN is None:
        return

    CONN.cursor().execute("UPDATE users SET password='%s' WHERE rowid=%d" % (password, user_id))
    CONN.commit()


def connect_db(db_path):
    global CONN

    if CONN is not None:
        return

    CONN = sqlite3.connect(db_path)


def close_connection():
    global CONN

    CONN.close()

    CONN = None

