from helpers import constants
import random


def is_letter(char: str) -> bool:
    return char.lower() in constants.CHARS


def is_punctuation(char: str) -> bool:
    return char in constants.PUNCTUATION_CHARS


def is_digit(char: str) -> bool:
    return char in [str(i) for i in range(10)]


def shuffle_str(string: str) -> str:
    return "".join(random.sample(string, len(string)))


def get_random_lower_char() -> str:
    return constants.CHARS[random.randint(0, len(constants.CHARS) - 1)]


def get_random_punctuation() -> str:
    return constants.PUNCTUATION_CHARS[random.randint(0, len(constants.PUNCTUATION_CHARS) - 1)]


def get_random_upper_char() -> str:
    return get_random_lower_char().upper()


def get_random_digit() -> str:
    return str(random.randint(0, 9))
